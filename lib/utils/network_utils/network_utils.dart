import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:trending_repos/constants/constants.dart';
import 'package:trending_repos/widgets/dialogs.dart';
import 'package:url_launcher/url_launcher.dart';

class NetworkUtils {
  static Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  static Future launchURL(BuildContext context, String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      String msg = kCouldNotLaunch + url;
      Dialogs.buildSnackBar(context, msg);
      throw msg;
    }
  }
}
