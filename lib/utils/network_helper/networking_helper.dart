import 'dart:convert';

import 'package:dio/dio.dart';

class NetworkingHelper {
  static Dio _dio = Dio();

  static Future<Map<String, dynamic>> getData(String endPoint,
      {Map<String, dynamic> body}) async {
    try {
      Response response = await _dio.get(endPoint, queryParameters: body);
      Map<String, dynamic> data = jsonDecode(response.toString());
      return data;
    } catch (e) {
      throw Exception(e);
    }
  }
}
