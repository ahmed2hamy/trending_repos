import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trending_repos/constants/constants.dart';
import 'package:trending_repos/views/home_screen/view/home_screen.dart';
import 'package:trending_repos/views/trending_page/cubit/trending_repos_cubit.dart';
import 'package:trending_repos/views/trending_page/cubit/trending_repos_scroll_to_top_cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: kAppTitle,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (_) => TrendingReposCubit(),
          ),
          BlocProvider(
            create: (_) => TrendingReposScrollToTopCubit(),
          ),
        ],
        child: HomeScreen(),
      ),
    );
  }
}
