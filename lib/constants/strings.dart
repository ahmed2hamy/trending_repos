part of 'constants.dart';

const String kAppTitle = "Trending Repos";
const String kTrendingRepos = "Trending Repos";
const String kSettings = "Settings";
const String kNoInternetConnection = "No internet connection.";
const String kCouldNotLaunch = "Could not launch ";
const String kNoRepositoryName = "[No Repository Name]";
const String kNoDescription = "[No Description]";
const String kNoOwnerName = "[No Owner Name]";
