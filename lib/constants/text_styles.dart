part of 'constants.dart';

TextStyle kTitleTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 18,
);
TextStyle kBlueTitleTextStyle = TextStyle(
  color: Colors.blue,
  fontSize: 18,
);
