import 'package:flutter/material.dart';
import 'package:trending_repos/constants/constants.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildSettingsIcon(context),
        SizedBox(height: 5),
        Text(kSettings, style: kBlueTitleTextStyle),
      ],
    );
  }

  Widget _buildSettingsIcon(BuildContext context) {
    final double iconSize = 50;
    return SizedBox(
      height: iconSize,
      width: iconSize,
      child: Icon(
        Icons.settings,
        size: iconSize,
        color: Theme.of(context).primaryColor,
      ),
    );
  }
}
