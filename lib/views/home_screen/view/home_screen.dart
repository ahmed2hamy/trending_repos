import 'package:flutter/material.dart';
import 'package:trending_repos/constants/constants.dart';
import 'package:trending_repos/views/home_screen/model/navigation_bar_item_model.dart';
import 'package:trending_repos/views/settings_page/view/settings_page.dart';
import 'package:trending_repos/views/trending_page/view/trending_page.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  List<NavigationBarItemModel> _navigationBarItems = [
    NavigationBarItemModel(
      title: kTrendingRepos,
      iconData: Icons.star,
      widget: TrendingPage(),
    ),
    NavigationBarItemModel(
      title: kSettings,
      iconData: Icons.settings,
      widget: SettingsPage(),
    ),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      initialIndex: 0,
      length: _navigationBarItems.length,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      title: Text(
        _navigationBarItems[_tabController.index].title,
        style: TextStyle(color: Colors.black),
      ),
    );
  }

  Widget _buildBody() {
    return TabBarView(
      physics: NeverScrollableScrollPhysics(),
      controller: _tabController,
      children: _navigationBarItems.map((e) => e.widget).toList(),
    );
  }

  Widget _bottomNavigationBar() {
    return BottomNavigationBar(
      onTap: (index) => setState(() => _tabController.animateTo(index)),
      currentIndex: _tabController.index,
      items: _navigationBarItems
          .map(
            (e) => BottomNavigationBarItem(
              icon: Icon(e.iconData),
              label: e.title,
            ),
          )
          .toList(),
    );
  }
}
