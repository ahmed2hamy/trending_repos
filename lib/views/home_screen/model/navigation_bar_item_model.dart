import 'package:flutter/material.dart';

class NavigationBarItemModel {
  final String title;
  final IconData iconData;
  final Widget widget;

  const NavigationBarItemModel({
    @required this.title,
    @required this.iconData,
    @required this.widget,
  });
}
