import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trending_repos/constants/constants.dart';
import 'package:trending_repos/utils/network_utils/network_utils.dart';
import 'package:trending_repos/views/trending_page/cubit/trending_repos_cubit.dart';
import 'package:trending_repos/views/trending_page/cubit/trending_repos_scroll_to_top_cubit.dart';
import 'package:trending_repos/views/trending_page/model/github_repo_model.dart';
import 'package:trending_repos/widgets/app_error_widget.dart';
import 'package:trending_repos/widgets/app_loading_widget.dart';
import 'package:trending_repos/widgets/dialogs.dart';

class TrendingPage extends StatefulWidget {
  @override
  _TrendingPageState createState() => _TrendingPageState();
}

class _TrendingPageState extends State<TrendingPage>
    with AutomaticKeepAliveClientMixin {
  final ScrollController _scrollController = ScrollController();
  List<RepoItem> _reposItems = [];
  int _currentPage = 1;

  @override
  bool get wantKeepAlive => true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _scrollController.addListener(_scrollListener);
    _getTrendingRepos();
  }

  void _scrollListener() {
    _getMoreRepos();

    _manipulateBackToTopButton();
  }

  void _getMoreRepos() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      _currentPage = _currentPage + 1;
      _getMoreTrendingRepos();
    }
  }

  Future _getMoreTrendingRepos() async {
    await context
        .read<TrendingReposCubit>()
        .getMoreTrendingRepos(_getRequestBody());
  }

  void _manipulateBackToTopButton() {
    if (_scrollController.offset >= 400) {
      context.read<TrendingReposScrollToTopCubit>().showBackToTopButton();
    } else {
      context.read<TrendingReposScrollToTopCubit>().hideBackToTopButton();
    }
  }

  Future _getTrendingRepos() async {
    await context
        .read<TrendingReposCubit>()
        .getTrendingRepos(_getRequestBody());
  }

  Map<String, dynamic> _getRequestBody() {
    return {
      "q": "created:>2017-10-22",
      "sort": "stars",
      "order": "desc",
      "page": _currentPage.toString(),
    };
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: _buildBody(),
      floatingActionButton: _floatingActionButton(),
    );
  }

  Widget _buildBody() {
    return BlocBuilder<TrendingReposCubit, TrendingReposState>(
      builder: (context, state) {
        if (state is LoadingState) {
          return AppLoadingWidget();
        } else if (state is LoadedState) {
          _reposItems.addAll(state.model.repoItems);
          return _buildReposListView(_reposItems);
        } else if (state is ErrorState) {
          Dialogs.buildSnackBar(context, state.errorMessage);
          return AppErrorWidget(_refreshData);
        } else {
          return Container();
        }
      },
    );
  }

  Widget _buildReposListView(List<RepoItem> repoItems) {
    return RefreshIndicator(
      onRefresh: _refreshData,
      child: ListView.separated(
        controller: _scrollController,
        itemCount: repoItems.length,
        separatorBuilder: (_, __) => Divider(),
        itemBuilder: (context, index) {
          if (index == repoItems.length - 1) {
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: AppLoadingWidget(),
            );
          }
          return RepoItemBuilder(repoItems[index]);
        },
      ),
    );
  }

  Future _refreshData() async {
    _currentPage = 0;
    _reposItems.clear();
    await _getTrendingRepos();
  }

  Widget _floatingActionButton() {
    return BlocBuilder<TrendingReposScrollToTopCubit, bool>(
        builder: (context, state) {
      return Visibility(
        visible: state,
        child: FloatingActionButton(
          onPressed: _scrollToTop,
          child: Icon(Icons.keyboard_arrow_up),
        ),
      );
    });
  }

  void _scrollToTop() {
    _scrollController.animateTo(
      0,
      duration: Duration(seconds: 2),
      curve: Curves.ease,
    );
  }
}

class RepoItemBuilder extends StatelessWidget {
  final RepoItem repo;

  const RepoItemBuilder(this.repo);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () =>
          NetworkUtils.launchURL(context, repo.htmlUrl ?? kPlaceholderUrl),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(repo.name ?? kNoRepositoryName, style: kTitleTextStyle),
            SizedBox(height: 20),
            Text(repo.description ?? kNoDescription),
            SizedBox(height: 20),
            _ownerDetails(),
          ],
        ),
      ),
    );
  }

  Widget _ownerDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Image.network(
              repo.owner.avatarUrl ?? kPlaceholderImageUrl,
              height: 30,
              width: 30,
            ),
            SizedBox(width: 5),
            Text(repo.owner.login ?? kNoOwnerName),
          ],
        ),
        Row(
          children: [
            Icon(Icons.star),
            SizedBox(width: 5),
            Text("${repo.stargazersCount ?? 0}"),
          ],
        ),
      ],
    );
  }
}
