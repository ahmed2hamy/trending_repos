import 'package:bloc/bloc.dart';

class TrendingReposScrollToTopCubit extends Cubit<bool> {
  TrendingReposScrollToTopCubit() : super(false);

  @override
  void onChange(Change<bool> change) {
    super.onChange(change);
    print(change);
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    print('$error, $stackTrace');
    super.onError(error, stackTrace);
  }

  void showBackToTopButton() {
    emit(true);
  }

  void hideBackToTopButton() {
    emit(false);
  }
}
