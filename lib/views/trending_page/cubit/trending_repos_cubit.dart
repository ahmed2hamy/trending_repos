import 'package:bloc/bloc.dart';
import 'package:trending_repos/views/trending_page/model/github_repo_model.dart';
import 'package:trending_repos/views/trending_page/repository/trending_page_repository.dart';

class TrendingReposCubit extends Cubit<TrendingReposState> {
  TrendingReposCubit() : super(InitialState());

  final TrendingPageRepository _repository = TrendingPageRepository();

  @override
  void onChange(Change<TrendingReposState> change) {
    super.onChange(change);
    print(change);
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    print('$error, $stackTrace');
    super.onError(error, stackTrace);
  }

  Future getTrendingRepos(Map<String, dynamic> body) async {
    try {
      emit(LoadingState());
      GithubRepoModel model = await _repository.getTrendingRepos(body);
      emit(LoadedState(model));
    } catch (e) {
      emit(ErrorState(e.toString()));
    }
  }

  Future getMoreTrendingRepos(Map<String, dynamic> body) async {
    try {
      GithubRepoModel model = await _repository.getTrendingRepos(body);
      emit(LoadedState(model));
    } catch (e) {
      emit(ErrorState(e.toString()));
    }
  }
}

abstract class TrendingReposState {}

class InitialState extends TrendingReposState {}

class LoadingState extends TrendingReposState {}

class LoadedState extends TrendingReposState {
  final GithubRepoModel model;

  LoadedState(this.model);
}

class ErrorState extends TrendingReposState {
  final String errorMessage;

  ErrorState(this.errorMessage);
}
