import 'package:trending_repos/constants/constants.dart';
import 'package:trending_repos/utils/network_helper/networking_helper.dart';
import 'package:trending_repos/utils/network_utils/network_utils.dart';
import 'package:trending_repos/views/trending_page/model/github_repo_model.dart';

class TrendingPageRepository {
  Future<GithubRepoModel> getTrendingRepos(Map<String, dynamic> body) async {
    return await NetworkUtils.checkConnectivity()
        ? _getTrendingReposRemotely(body)
        : _getTrendingReposLocally();
  }

  Future<GithubRepoModel> _getTrendingReposRemotely(
      Map<String, dynamic> body) async {
    Map<String, dynamic> json =
        await NetworkingHelper.getData(kSearchRepositoriesEndpoint, body: body);
    GithubRepoModel model = GithubRepoModel.fromJson(json);
    return model;
  }

  Future<GithubRepoModel> _getTrendingReposLocally() async {
    /// For future offline mode implementation
    throw Exception(kNoInternetConnection);
  }
}
