import 'package:flutter/material.dart';

class Dialogs {
  static buildSnackBar(BuildContext context, String text, {int duration = 3}) {
    SnackBar snackBar = SnackBar(
      content: Text(text),
      duration: Duration(seconds: duration),
    );
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Scaffold.of(context).showSnackBar(snackBar);
    });
  }
}
