import 'package:flutter/material.dart';

class AppErrorWidget extends StatelessWidget {
  final Function refreshCallback;

  const AppErrorWidget(this.refreshCallback);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: IconButton(
        iconSize: 50,
        color: Theme.of(context).primaryColor,
        icon: Icon(Icons.refresh),
        onPressed: refreshCallback,
      ),
    );
  }
}
